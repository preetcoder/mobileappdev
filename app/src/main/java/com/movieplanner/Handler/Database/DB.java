package com.movieplanner.Handler.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Harpreet on 21/04/2019
 **/
public class DB extends SQLiteOpenHelper {

        // If you change the database schema, you must increment the database version.
        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "eventPlanner.db";
        public static final String TABLE1_NAME = "events";
        public static final String TABLE2_NAME = "movies";
        public static final String TABLE3_NAME = "attendees";


    private static final String SQL_CREATE_EVENTS =
            "CREATE TABLE " + TABLE1_NAME + " (" +
                    "ID INTEGER PRIMARY KEY autoincrement," +
                    " eventTitle TEXT NOT NULL," +
                    " startDate TEXT NOT NULL, "+
                    " endDate TEXT NOT NULL, "+
                    " venue TEXT NOT NULL, "+
                    " location TEXT NOT NULL, "+
                    " attendees TEXT, "+
                    " movieID INTEGER );";


    private static  final String SQL_CREATE_MOVIE =
            "CREATE TABLE "+ TABLE2_NAME + " (ID INTEGER PRIMARY KEY autoincrement,"
                    +" movieName TEXT NOT NULL," +
                    " movieYear TEXT NOT NULL, "+
                    " movieposter TEXT );";

    private static final String SQL_DELETE_EVENTS =
            "DROP TABLE IF EXISTS " + TABLE1_NAME;

    private static final String SQL_DELETE_MOVIE =
            "DROP TABLE IF EXISTS " + TABLE2_NAME;

        public DB(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
        public void onCreate(SQLiteDatabase db) {
            // first create movies then events because events has reference to movie

            db.execSQL(SQL_CREATE_MOVIE);

            db.execSQL(SQL_CREATE_EVENTS);
        }
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // This database is only a cache for online data, so its upgrade policy is
            // to simply to discard the data and start over
            db.execSQL(SQL_DELETE_EVENTS);
            db.execSQL(SQL_DELETE_MOVIE);
            onCreate(db);
        }
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }

}
