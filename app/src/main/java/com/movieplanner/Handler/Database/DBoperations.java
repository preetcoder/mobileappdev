package com.movieplanner.Handler.Database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.movieplanner.Model.Attendees;
import com.movieplanner.Model.Movie;
import com.movieplanner.Model.MovieEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Harpreet on 22/04/2019
 **/
public class DBoperations {

    public void insertEvents(SQLiteDatabase db,  List<MovieEvent> eventVal){

        for(int i=0;i<eventVal.size();i++){

            String sql = "INSERT INTO events (eventTitle, startDate, endDate, venue, location) VALUES " +
                    "('"+eventVal.get(i).getEventTitle()+"', '"+eventVal.get(i).getStartDate()+"', '"+
                    eventVal.get(i).getEndDate()+"', '"+eventVal.get(i).getVenue()+"', '"+eventVal.get(i).getLocation()+"')";

           // Log.d("sql",sql);

            db.execSQL(sql);

        }
    }

    // method for adding movies from file
    public void insertMovies(SQLiteDatabase db,  Movie movieVals){
        String sql = "INSERT INTO movies (movieName, movieYear, movieposter) VALUES "+"('"+movieVals.getTitle()+
                "','"+movieVals.getYear()+"','"+movieVals.getPoster()+"');";

        db.execSQL(sql);

    }

    // method for add new event
     public boolean addNewEvent(SQLiteDatabase db,  MovieEvent newEventVal){
         String sql;

        if(newEventVal!= null) {

            if(newEventVal.getContacts() == null){
                sql = "INSERT INTO events (eventTitle, startDate, endDate, venue, location, movieID) VALUES ('" + newEventVal.getEventTitle() + "'," +
                        " '" + newEventVal.getStartDate() + "', '" + newEventVal.getEndDate() + "'," +
                        " '" + newEventVal.getVenue() + "', '" + newEventVal.getLocation() + "'," +
                        (Integer.parseInt(newEventVal.getMoviedetails().getId()) + 1) + ");";
            }


             else{

                 String attendees = "";
                 // comma seperated attendees email
                for(int i=0;i<newEventVal.getContacts().size();i++){
                    attendees = attendees + newEventVal.getContacts().get(i).getEmail() + ",";
                }

                // removing last comma


                sql = "INSERT INTO events (eventTitle, startDate, endDate, venue, location, movieID, attendees) VALUES ('"+newEventVal.getEventTitle()+"'," +
                        " '"+newEventVal.getStartDate()+"', '"+newEventVal.getEndDate()+"'," +
                        " '"+newEventVal.getVenue()+"', '"+newEventVal.getLocation() + "'," +
                        (Integer.parseInt(newEventVal.getMoviedetails().getId())) + ",'" + attendees.substring(0, attendees.lastIndexOf(",")) + "');";
            }
            // Log.d("sql",sql);

            db.execSQL(sql);

            return true;
        }




        return false;
     }

    // method for checking if DB has value or not
    public boolean checkmyDatabaseEmpty(SQLiteDatabase db){

        String sql = "SELECT count(*) from events ";
        Cursor mcursor = db.rawQuery(sql, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);

      //  Log.d("valDB", Integer.toString(icount));
        if(icount > 0){
            return true;
        }
        return false;
    }

    // method for checking if DB has value or not
    public boolean checkmyMoviesDatabaseEmpty(SQLiteDatabase db){

        String sql = "SELECT count(*) from movies ";
        Cursor mcursor = db.rawQuery(sql, null);
        mcursor.moveToFirst();
        int icount = mcursor.getInt(0);

        //  Log.d("valDB", Integer.toString(icount));
        if(icount > 0){
            return true;
        }

        return false;

    }

    // method to read events from db and pass in MovieEvent type of list

    public List<MovieEvent> readEventsfromDB(SQLiteDatabase db){
        List<MovieEvent> allEventsfromDB = new ArrayList<>();

        String sql = "SELECT * from events";
        Cursor readCursor = db.rawQuery(sql,null);

        if(readCursor.moveToFirst()){

            //loop through all events
            do {


                int SingleEventId = readCursor.getInt(0);
                String SingleEventTitle = readCursor.getString(1);
                String SingleEventStartDate = readCursor.getString(2);
                String SingleEventEndDate = readCursor.getString(3);
                String SingleEventVenue = readCursor.getString(4);
                String SingleEventLocation = readCursor.getString(5);
                String SingleEventMovie = readCursor.getString(7);

                // @Harpreet need to work
                String SingleEventAttendees = readCursor.getString(6);


                // get Movie object from SingleEventMovie

                List<Movie> MovieDetails = readMoviesfromDB(db,SingleEventMovie);
                List<Attendees> attendeesObj = new ArrayList<>();
                // get Attendees data and make object of attendees
                if( SingleEventAttendees != null){

                    String[] attendeesData = SingleEventAttendees.split(",");

                    for (int i = 0;i<attendeesData.length;i++){
                        attendeesObj.add(new Attendees(attendeesData[i]));
                    }
                }

                MovieEvent SingleEvent;

                     if(SingleEventMovie != null && !SingleEventMovie.isEmpty()){

                        // attendees coz there will always be a movie if attendees available
                        if(attendeesObj != null){
                            SingleEvent = new MovieEvent(Integer.toString(SingleEventId), SingleEventTitle, SingleEventStartDate,
                                    SingleEventEndDate, SingleEventVenue, SingleEventLocation, MovieDetails.get(0), attendeesObj);
                        }
                        else{
                            SingleEvent = new MovieEvent(Integer.toString(SingleEventId), SingleEventTitle, SingleEventStartDate,
                                    SingleEventEndDate, SingleEventVenue, SingleEventLocation, MovieDetails.get(0));
                        }

                    }
                    else{
                         SingleEvent = new MovieEvent(Integer.toString(SingleEventId), SingleEventTitle, SingleEventStartDate,
                                SingleEventEndDate, SingleEventVenue, SingleEventLocation);
                    }




                allEventsfromDB.add(0,SingleEvent);
            }
            while(readCursor.moveToNext());
        }


        return allEventsfromDB;
    }

        // method for reading movies
        public List<Movie> readMoviesfromDB(SQLiteDatabase db, String value) {
            List<Movie> allMovies = new ArrayList<>();
            String sql;
            if("multi".equals(value)){
                sql = "SELECT * from movies ";
            }
            else{
                sql = "SELECT * from movies WHERE ID = "+ value;
            }

            Cursor moviecursor = db.rawQuery(sql, null);
            if (moviecursor.moveToFirst()) {

                //loop through all events
                do {

                    int movieId = moviecursor.getInt(0);
                    String movieTitle = moviecursor.getString(1);
                    String movieYear = moviecursor.getString(2);
                    String moviePoster = moviecursor.getString(3);

                    Movie SingleMovie = new Movie(Integer.toString(movieId), movieTitle, movieYear, moviePoster);

                    allMovies.add(0,SingleMovie);
                }
                while (moviecursor.moveToNext());


            }

            return allMovies;
        }

        // Single event update
    public boolean updateEvent(SQLiteDatabase db, String EventID, String eventTitle, String eventStartDate, String eventEndDate,
                               String eventVenue, String eventLocation , String eventMovie, List<Attendees> attendeesList){
        ContentValues cv = new ContentValues();

        String attendees= "";

        cv.put("eventTitle",eventTitle);
        cv.put("startDate",eventStartDate);
        cv.put("endDate",eventEndDate);
        cv.put("venue",eventVenue);
        cv.put("location",eventLocation);
        cv.put("movieID",Integer.parseInt(eventMovie));

        // Attendees comma seperated
        if(attendeesList.size() > 0){
            for(int i =0;i<attendeesList.size();i++){
                attendees = attendees + attendeesList.get(i).getEmail() + ",";
            }
            cv.put("attendees",attendees.substring(0, attendees.lastIndexOf(",")));
        }


        if(!cv.equals(null)){
            db.update("events", cv, "ID ="+EventID, null);
            return true;
        }


        return false;
    }

    // method for event deletion

    public boolean deleteEvents(SQLiteDatabase db, String EventID){

        if(!EventID.equals(null)){
            db.delete("events","ID="+EventID ,null) ;
            return true;
        }

        return false;

    }

}
