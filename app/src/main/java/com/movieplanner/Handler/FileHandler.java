package com.movieplanner.Handler;

import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.movieplanner.Handler.Database.DBoperations;
import com.movieplanner.Model.Movie;
import com.movieplanner.Model.MovieEvent;
import com.movieplanner.R;
import com.movieplanner.View.ListViewFragment;
import com.movieplanner.View.MainActivity;
import com.movieplanner.View.ViewMovies;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class FileHandler {

    public MovieEvent newEvent;

    public List<Movie> parseMoviesFile(Context context) {
        // resource reference to events.txt in res/raw/ folder of your project

        List<Movie> movies = new ArrayList<>();
        // check if data in DB

        DBoperations DBoperation = new DBoperations();

        SQLiteDatabase mMovies = MainActivity.sqlitehelper.getReadableDatabase();

        if(!DBoperation.checkmyMoviesDatabaseEmpty(mMovies)){

            // add movies to Database
            SQLiteDatabase mDatabase = MainActivity.sqlitehelper.getWritableDatabase();

            // load in db after file operations
            try (Scanner scanner = new Scanner(context.getResources().openRawResource(R.raw.movies))) {
                // match comma and 0 or more whitespace OR trailing space and newline

                scanner.useDelimiter(",\\s*|\\s*\\n+");
                //loop through the lines and get all instance values
                while (scanner.hasNext()) {
                    String line = scanner.nextLine();
                    String[] splitText = line.split(",");

                    // Removing double quotes
                    String id = splitText[0].replaceAll("^\"|\"$", "");
                    String title = splitText[1].replaceAll("^\"|\"$", "");
                    String year = splitText[2].replaceAll("^\"|\"$", "");
                    String poster = splitText[3].replaceAll("^\"|\"$", "");

                    boolean add = movies.add(new Movie(id, title, year, poster));

                    // in sert in db
                    DBoperation.insertMovies(mDatabase, new Movie(id, title, year, poster));
                }
            } catch (Resources.NotFoundException e) {
            }


        }
        else{
            movies = DBoperation.readMoviesfromDB(mMovies, "multi");
        }


        return movies;
    }

    private void populateMovies(Context context){
        List<Movie> moviesData = parseMoviesFile(context);
        Movie myList;
        ViewMovies.list.clear();
        for (int i = 0; i <  moviesData.size(); i++) {
            myList = new Movie(
                    moviesData.get(i).getId(),
                    moviesData.get(i).getTitle(),
                    moviesData.get(i).getYear(),
                    moviesData.get(i).getPoster()
            );

            ViewMovies.list.add(myList);
        }
        //Collections.copy(ViewMovies.list, moviesData);
    }


    public List<MovieEvent> parseEventsFile(Context context) {
        // resource reference to events.txt in res/raw/ folder of your project

        List<MovieEvent> eventsList = new ArrayList<>();

        // check if database has data

        DBoperations DBoper = new DBoperations();

        SQLiteDatabase mReadDatabase = MainActivity.sqlitehelper.getReadableDatabase();

        if(!DBoper.checkmyDatabaseEmpty(mReadDatabase)){
            // if empty only then populate
           // Log.i("val","Files");
        try (Scanner scanner = new Scanner(context.getResources().openRawResource(R.raw.events))) {
            // match comma and 0 or more whitespace OR trailing space and newline

            scanner.useDelimiter(",\\s*|\\s*\\n+");
            //loop through the lines and get all instance values
            while (scanner.hasNext()) {
                // fetch data from each line into an array
                String line = scanner.nextLine();
                String[] splitText = line.split(",");

                // Removing double quotes from each value
                String id = splitText[0].replaceAll("^\"|\"$", "");
                String title = splitText[1].replaceAll("^\"|\"$", "");
                String startDate = splitText[2].replaceAll("^\"|\"$", "");
                String endDate = splitText[3].replaceAll("^\"|\"$", "");
                String venue = splitText[4].replaceAll("^\"|\"$", "");
                String location1 = splitText[5] + "%2C"+splitText[6].replaceAll("\\s","");
                String location = location1.replaceAll("^\"|\"$", "");

                eventsList.add(new MovieEvent(id, title,  startDate, endDate,venue, location));

                // add lists to AllEvents arraylist after object creation

                ListViewFragment listViewFragment = new ListViewFragment();
                listViewFragment.AllEvents.add(new MovieEvent(id, title,  startDate, endDate, venue, location));



            }
        } catch (Resources.NotFoundException e) {
        }


            // add events to Database
            SQLiteDatabase mDatabase = MainActivity.sqlitehelper.getWritableDatabase();
            DBoper.insertEvents(mDatabase, eventsList);
        }
        else{



            // get data from db and show
            eventsList = DBoper.readEventsfromDB(mReadDatabase);

            // add lists to AllEvents arraylist after object creation
            ListViewFragment listViewFragment = new ListViewFragment();
            listViewFragment.AllEvents.clear();
            for (int i=0;i<eventsList.size();i++){

                if(eventsList.get(i).getMoviedetails() != null){
                    // if attendees
                    if(eventsList.get(i).getContacts() != null){
                        listViewFragment.AllEvents.add(new MovieEvent(eventsList.get(i).getEventId(), eventsList.get(i).getEventTitle(),
                                eventsList.get(i).getStartDate(), eventsList.get(i).getEndDate(),eventsList.get(i).getVenue(),
                                eventsList.get(i).getLocation(), eventsList.get(i).getMoviedetails(), eventsList.get(i).getContacts()));
                    }
                    else{
                        listViewFragment.AllEvents.add(new MovieEvent(eventsList.get(i).getEventId(), eventsList.get(i).getEventTitle(),
                                eventsList.get(i).getStartDate(), eventsList.get(i).getEndDate(),eventsList.get(i).getVenue(),
                                eventsList.get(i).getLocation(), eventsList.get(i).getMoviedetails()));
                    }

                }
                else{
                    listViewFragment.AllEvents.add(new MovieEvent(eventsList.get(i).getEventId(), eventsList.get(i).getEventTitle(),
                            eventsList.get(i).getStartDate(), eventsList.get(i).getEndDate(),eventsList.get(i).getVenue(),
                            eventsList.get(i).getLocation()));
                }

            }


        }

        //Log.i("Filehandler", MainActivity.sqlitehelper.toString());
        // insertEvents

        // check if movies in database if yes then load.
        populateMovies(context);

        return eventsList;
    }




}
