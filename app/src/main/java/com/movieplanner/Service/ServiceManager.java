package com.movieplanner.Service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class ServiceManager
{
    private static ServiceManager singletonInstance = null;

    private PendingIntent alarmIntent;
    private AlarmManager alarmManager;

    public static ServiceManager getSingletonInstance(Context context)
    {
        if (singletonInstance == null)
        {
            singletonInstance = new ServiceManager(context);
        }
        return singletonInstance;
    }

    private ServiceManager(Context context)
    {
    }

    /**
     * Start the DistanceMatrixIntentService in the background to periodically
     * check for travel time to each event and notify if necessary.
     */
    public void startBackgroundDistanceMatrixService(Context context)
    {
        if (isAlarmRunning(context))
        {
            stopBackgroundDistanceMatrixService();
        }
        Intent intent = new Intent(context.getApplicationContext(),
                AlarmReceiver.class);
       // Toast.makeText(context, "Google distance matrix started", Toast.LENGTH_LONG).show();

        alarmIntent = PendingIntent.getBroadcast(context,
                AlarmReceiver.REQUEST_CODE, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        alarmManager = (AlarmManager) context.getSystemService(
                Context.ALARM_SERVICE);

        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(context.getApplicationContext());
        long interval = prefs.getLong("distance_check_freq_interval",
                DistanceMatrixIntentService.DEFAULT_INTERVAL_MS);

        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime(), interval, alarmIntent);
    }

    /**
     * Checks if the background service is already running.
     *
     * @return True if the alarm is already set to repeat the background
     *         service periodically, otherwise false.
     */
    private boolean isAlarmRunning(Context context)
    {
        Intent intent = new Intent(context.getApplicationContext(),
                AlarmReceiver.class);

        return PendingIntent.getBroadcast(context.getApplicationContext(),
                AlarmReceiver.REQUEST_CODE, intent,
                PendingIntent.FLAG_NO_CREATE)
                != null;
    }

    /**
     * Stop the background service.
     */
    private void stopBackgroundDistanceMatrixService()
    {
        if (alarmManager != null && alarmIntent != null)
        {
            alarmManager.cancel(alarmIntent);
            alarmIntent.cancel();
        }
    }
}
