package com.movieplanner.Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * For the scheduling of an IntentService to run at set interval.
 */
public class AlarmReceiver extends BroadcastReceiver
{
    public static final int REQUEST_CODE = 123456;

    @Override
    public void onReceive(Context context, Intent intent)
    {
        context.startService(
                new Intent(context, DistanceMatrixIntentService.class));

    }
}
