package com.movieplanner.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.widget.Toast;
import android.content.BroadcastReceiver;
import android.net.NetworkInfo;

import com.movieplanner.View.MainActivity;

public class ConnectivityMgr extends BroadcastReceiver {

    private Context context;
    private Intent intent;

    private ServiceManager serviceManager;

    @Override
    public void onReceive(Context context, Intent intent) {

       this.context = context;
       this.intent = intent;


            ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo ni = manager.getActiveNetworkInfo();
            doSomethingOnNetworkChange(ni);
        }


    public void doSomethingOnNetworkChange(NetworkInfo dd){
        if(dd == null){
            MainActivity.checkNetworkStatus = false;
            Toast.makeText(context, "Not Connected", Toast.LENGTH_SHORT).show();
            return;
        }
        if(dd.isConnected()){
            MainActivity.checkNetworkStatus = true;
            Toast.makeText(context, "Network Connected", Toast.LENGTH_SHORT).show();
            serviceManager = ServiceManager.getSingletonInstance(context);
            serviceManager.startBackgroundDistanceMatrixService(context);
        }

    }

}
