package com.movieplanner.Service;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Icon;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.movieplanner.Model.MovieEvent;
import com.movieplanner.R;
import com.movieplanner.View.CancelEventNotificationActivity;
import com.movieplanner.View.ListViewFragment;

import static com.movieplanner.R.drawable.baseline_cancel_black_24dp;

public class RemindNotificationDisplayReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        String eventID = intent.getStringExtra("eventid");
        MovieEvent event = new MovieEvent();
        // findout the object from id

        for(int i=0; i< ListViewFragment.AllEvents.size();i++){
            if(eventID.equals(ListViewFragment.AllEvents.get(i).getEventId())){
                event = ListViewFragment.AllEvents.get(i);
            }
        }


        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(context);

        long reminderFreq = prefs.getLong("remind_again_check_interval",
                DistanceMatrixIntentService.DEFAULT_REMIND_AGAIN_INTERVAL_MS);

        int notificationId = generateNotificationId();
        Log.i("gennotiID", Integer.toString(notificationId));

        Intent dismissActionIntent = new Intent(context, DismissActionReceiver.class);
        dismissActionIntent.putExtra("noti_id", notificationId);
        dismissActionIntent.putExtra("eventName", event.getEventTitle());


        Intent cancelActionIntent = new Intent(context, CancelEventNotificationActivity.class);
        cancelActionIntent.putExtra("eventID", event.getEventId());
        cancelActionIntent.putExtra("eventName", event.getEventTitle());
        cancelActionIntent.putExtra("eventVenue", event.getVenue());

        PendingIntent cancelDismissIntent = PendingIntent.getActivity(context, notificationId, cancelActionIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        Intent remindAgainIntent = new Intent(context, RemindAgainNotificationReceiver.class);
        remindAgainIntent.putExtra("noti_id", notificationId);
        remindAgainIntent.putExtra("remi_event_title", event.getEventTitle());
        remindAgainIntent.putExtra("remi_event_id", event.getEventId());

        PendingIntent remindIntent = PendingIntent.getBroadcast(context, notificationId, remindAgainIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        PendingIntent pendingDismissIntent = PendingIntent.getBroadcast(context, notificationId, dismissActionIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Action dismiss = new Notification.Action.Builder(
                0,
                "Dismiss",
                pendingDismissIntent).build();

        Notification.Action cancel = new Notification.Action.Builder(
                0,
                "Cancel",
                cancelDismissIntent).build();

        Notification.Action remindAgain = new Notification.Action.Builder(
                0,
                "Remind in " + (reminderFreq / 60000) + " minutes",
                remindIntent).build();

        //todo move to seperate method

        // Create the notification itself
        Notification notification
                = new Notification.Builder(context)
                .setSmallIcon(R.drawable.ic_event_notification)
                .addAction(dismiss)
                .addAction(cancel)
                .addAction(remindAgain)
                .setContentTitle(context.getResources().getString(
                        R.string.notification_title, event.getEventTitle()))
                .setAutoCancel(true)
                .build();

        // Display the notification
        NotificationManagerCompat notifyMgr
                = NotificationManagerCompat.from(context);
        notifyMgr.notify(notificationId, notification);
    }

    private int generateNotificationId() {
        long now = (int) (Math.random() * 49);
        String nowStr = String.valueOf(now);

        return Integer.parseInt(nowStr);
    }
}
