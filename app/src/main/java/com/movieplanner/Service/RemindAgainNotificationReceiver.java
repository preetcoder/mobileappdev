package com.movieplanner.Service;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.preference.PreferenceManager;

public class RemindAgainNotificationReceiver extends BroadcastReceiver {

    public static final int REQUEST_CODE = 12345;

    private AlarmManager alarmManager;
    private PendingIntent alarmIntent;


    @Override
    public void onReceive(Context context, Intent intent) {

        //todo send these values to reminder notifcation through intent to display event title
        int noti_id = intent.getIntExtra("noti_id",0);

        String eventTitle = intent.getStringExtra("remi_event_title");
        String eventID = intent.getStringExtra("remi_event_id");


        Intent newIntent = new Intent(context.getApplicationContext(),
                RemindNotificationDisplayReceiver.class);
        newIntent.putExtra("eventid",eventID);


        NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        long threshold = prefs.getLong("remind_again_check_interval",
                DistanceMatrixIntentService.DEFAULT_REMIND_AGAIN_INTERVAL_MS);

        notificationManager.cancel(noti_id);

        alarmIntent = PendingIntent.getBroadcast(context,
                REQUEST_CODE, newIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        alarmManager = (AlarmManager) context.getSystemService(
                Context.ALARM_SERVICE);

        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,(SystemClock.elapsedRealtime() + threshold), alarmIntent);
    }
}
