package com.movieplanner.Service;

import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Icon;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import com.movieplanner.Model.MovieEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import com.movieplanner.R;
import com.movieplanner.View.CancelEventNotificationActivity;
import com.movieplanner.View.ListViewFragment;

public class DistanceMatrixIntentService extends IntentService
{
    public static final long DEFAULT_INTERVAL_MS = 5 * 60 * 1000; // 5min
    public static final long DEFAULT_THRESHOLD_MS = 15 * 60 * 1000; // 15min
    public static final long DEFAULT_REMIND_AGAIN_INTERVAL_MS = 1 * 60 * 1000; // 1min


    private static final String LOG_TAG = DistanceMatrixIntentService.class
            .getSimpleName();
    private static final String DISTANCE_MATRIX_URL
            = "https://maps.googleapis.com/maps/api/distancematrix/json"
            + "?origins=%s&destinations=%s&mode=driving&key=AIzaSyAH3W4WhHVU1oDp8yVkavOaDdQUhReN85E";



    public DistanceMatrixIntentService()
    {
        super("DistanceMatrixIntentService");

    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        Log.i(LOG_TAG, "Running distance matrix service");

               // Check network availability
        if (!isOnline())
        {
            Log.i(LOG_TAG, "No network connectivity");
            return;
        }

        // Get current location
        Location currentLocation = LocationTracker.getLocation(
                getApplicationContext());

        if (currentLocation == null)
        {
            Log.i(LOG_TAG, "Couldn't get current location");
            return;
        }

        try
        {
            /*
             * Call Google Distance Matrix API to calculate driving time
             * to each event
             */
            Map<MovieEvent, Long> travelTimeToEvents = getTravelTimeToEvents(
                    currentLocation);

            /*
             * If any meet the criteria ([travelTime + threshold] or less
             * until event start), notify user
             */
            SharedPreferences prefs = PreferenceManager
                    .getDefaultSharedPreferences(getApplicationContext());
            long threshold = prefs.getLong("notification_threshold",
                    DistanceMatrixIntentService.DEFAULT_THRESHOLD_MS);

            long reminderFreq = prefs.getLong("remind_again_check_interval",
                    DistanceMatrixIntentService.DEFAULT_REMIND_AGAIN_INTERVAL_MS);

            buildNotifications(travelTimeToEvents,  reminderFreq, threshold);

        } catch (Exception e)
        {
            Log.e(LOG_TAG, e.getMessage(), e);
        }

    }

    private void buildNotifications(Map<MovieEvent, Long> travelTimeToEvents, long reminderFreq, long threshold){
        for (Map.Entry<MovieEvent, Long> pair
                : travelTimeToEvents.entrySet())
        {
            MovieEvent event = pair.getKey();
            long travelTime = pair.getValue();

            long eventStartMs = getEventStartTimeinMillis(event.getEventStartTime());
            long travelTimeMs = travelTime * 1000;
            long currentTime = TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis());

            // skip if doesn't meet criteria
                if(!((currentTime + threshold/60000 + travelTime/60) == eventStartMs) ){

                    continue;
                }

            // Create intent for clicking on notification

            int notificationId = generateNotificationId();
            Log.i("gennotiID",Integer.toString(notificationId));

            Intent dismissActionIntent = new Intent(getApplicationContext(), DismissActionReceiver.class);
            dismissActionIntent.putExtra("noti_id", notificationId);
            dismissActionIntent.putExtra("eventName", event.getEventTitle());


            Intent cancelActionIntent = new Intent(getApplicationContext(), CancelEventNotificationActivity.class);
            cancelActionIntent.putExtra("eventID", event.getEventId());
            cancelActionIntent.putExtra("noti_id", notificationId);
            cancelActionIntent.putExtra("eventName", event.getEventTitle());
            cancelActionIntent.putExtra("eventVenue", event.getVenue());

            PendingIntent cancelDismissIntent = PendingIntent.getActivity(getApplicationContext(), notificationId, cancelActionIntent,PendingIntent.FLAG_CANCEL_CURRENT);

            Intent remindAgainIntent = new Intent(getApplicationContext(), RemindAgainNotificationReceiver.class);
            remindAgainIntent.putExtra("noti_id", notificationId);
            remindAgainIntent.putExtra("remi_event_title", event.getEventTitle());
            remindAgainIntent.putExtra("remi_event_id", event.getEventId());

            PendingIntent remindIntent = PendingIntent.getBroadcast(getApplicationContext(), notificationId, remindAgainIntent,PendingIntent.FLAG_UPDATE_CURRENT);

            PendingIntent pendingDismissIntent = PendingIntent.getBroadcast(getApplicationContext(), notificationId, dismissActionIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            Notification.Action dismiss = new Notification.Action.Builder(
                    0,
                    "Dismiss",
                    pendingDismissIntent).build();

            Notification.Action cancel = new Notification.Action.Builder(
                    0,
                    "Cancel",
                    cancelDismissIntent).build();

            Notification.Action remindAgain = new Notification.Action.Builder(
                    0,
                    "Remind in " + (reminderFreq/60000) +  " minutes",
                    remindIntent).build();

            //todo move to seperate method

            // Create the notification itself
            Notification notification
                    = new Notification.Builder(this)
                    .setSmallIcon(R.drawable.ic_event_notification)
                    .addAction(dismiss)
                    .addAction(cancel)
                    .addAction(remindAgain)
                    .setContentTitle(getResources().getString(
                            R.string.notification_title, event.getEventTitle()))
                    .setContentText(getResources().getString(
                            R.string.notification_text,
                            millisToDisplay(eventStartMs - currentTime),
                            secondsToDisplay(travelTime)))
                    .setAutoCancel(true)
                    .build();

            // Display the notification
            NotificationManagerCompat notifyMgr
                    = NotificationManagerCompat.from(this);
            notifyMgr.notify(notificationId, notification);
        }
    }
    /**
     * Check if the device has network connectivity.
     *
     * @return True if there is network connectivity, otherwise false.
     */
    private boolean isOnline()
    {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * Make a call to the Google Distance Matrix API to determine how long it
     * would take to travel by car from the user's current location to each
     * event individually.
     */
    private Map<MovieEvent, Long> getTravelTimeToEvents(Location currentLocation)
            throws IOException, JSONException
    {
        List<MovieEvent> events = ListViewFragment.AllEvents;

        if (events.isEmpty())
        {
            return new HashMap<>();
        }

        // Construct 'destinations' parameter for URL
        String dests = "";
        for (int i = 0; i < events.size(); i++)
        {
            dests += events.get(i).getLocation();
            if (i != events.size() - 1)
            {
                // Add separator for multiple destinations
                dests += "%7C";
            }
        }
        // Construct 'origins' parameter for URL
        String origin = currentLocation.getLatitude() + ","
                + currentLocation.getLongitude();
        // Construct URL for distance matrix API call
        String urlStr = String.format(DISTANCE_MATRIX_URL, origin, dests);

        // Perform distance matrix API call
        URL url = new URL(urlStr);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.connect();

        int responseCode = connection.getResponseCode();
        if (responseCode != 200)
        {
            throw new IOException();
        }

        Map<MovieEvent, Long> travelTimeToEvents = new HashMap<>();
        /*
         * Parse JSON result of distance matrix API call
         * A Map is constructed linking event ID to travel time to event
         */
        String jsonStr = new Scanner(connection.getInputStream())
                .useDelimiter("\\A").next();
        JSONObject jsonResult =  new JSONObject(jsonStr);

        if(jsonResult.get("status").equals("OK")){

            JSONArray distMatrixElements = jsonResult.getJSONArray("rows")
                    .getJSONObject(0).getJSONArray("elements");

           //fetch time taken to reach the place in milliseconds for each of the event
            for (int i = 0; i < events.size(); i++)
            {
                travelTimeToEvents.put(events.get(i),
                        distMatrixElements.getJSONObject(i)
                                .getJSONObject("duration").getLong("value"));
            }
        }
        else{
            throw new IOException();
        }

        return travelTimeToEvents;
    }

    //fetch event start time and convert into milliseconds
    private long getEventStartTimeinMillis(String eventStart){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        Date date = null;
        try {
            date = sdf.parse(eventStart);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long millis = date.getTime();

        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);

        return minutes;
    }

    /**
     * Generate a random notification ID based on the current system time.
     *
     */
    private int generateNotificationId()
    {
        long now = (int)(Math.random() * 49);
        String nowStr = String.valueOf(now);

        return Integer.parseInt(nowStr);
    }

    /**
     * Convert milliseconds value into a String showing it in hours, minutes,
     * and seconds.
     *
     */
    private static String millisToDisplay(long millis)
    {
        long hours = TimeUnit.MILLISECONDS.toHours(millis);
        long minutes = (TimeUnit.MILLISECONDS.toMinutes(millis) % 60);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis) % 60;

        // TODO Make this nicer (i.e. ignore fields that are 0)
        return String.format(Locale.getDefault(), "%dh, %dm, %ds",
                hours, minutes, seconds);
    }

    /**
     * Helper method for {@link #millisToDisplay(long)} to accept values in
     * seconds.
     */
    private static String secondsToDisplay(long secs)
    {
        return millisToDisplay(secs * 1000);
    }
}
