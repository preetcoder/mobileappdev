package com.movieplanner.View;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import com.movieplanner.Model.MovieEvent;
import com.movieplanner.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class MapView extends Fragment implements OnMapReadyCallback
{
    private GoogleMap map;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        View v = inflater.inflate(R.layout.map_view, container, false);

        SupportMapFragment mapFragment
                = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        return v;
    }

    // refresh fragment upon reload
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Refresh fragment
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }


    public void onCreate(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        super.onCreate(savedInstanceState);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     */
    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        map = googleMap;
        map.clear();

        List<MovieEvent> firstThree = getSoonestEvents();

        String latitude[]= new String[firstThree.size()];
        String longitude[] = new String[firstThree.size()];

        for(int i =0; i<firstThree.size();i++){
            latitude[i]= firstThree.get(i).getEventLocationLatitude();
            longitude[i]= firstThree.get(i).getEventLocationLongitude();
        }

       //locations

        for (int i = 0; i < firstThree.size(); i++) {

            // Adding a marker
            MarkerOptions marker = new MarkerOptions().position(
                    new LatLng(Double.parseDouble(latitude[i]), Double.parseDouble(longitude[i])))
                    .title(firstThree.get(i).getEventTitle());


            // changing marker color
            if (i == 0)
                marker.icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_RED));
            if (i == 1)
                marker.icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_RED));
            if (i == 2)
                marker.icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_RED));

            googleMap.addMarker(marker);

            // Move the camera to last position with a zoom level
            if (i == 1) {
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(Double.parseDouble(latitude[1]),
                                Double.parseDouble(longitude[1]))).zoom(15).build();

                googleMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));
            }
        }
        googleMap.getUiSettings().setZoomControlsEnabled(true);
    }

    // fetch all events , sort them and check if they are greater than current time based on which the soonest events will be shown on the map
    private List<MovieEvent> getSoonestEvents(){
        List<MovieEvent> movieEvents = ListViewFragment.AllEvents;
        Collections.sort(movieEvents);

        List<MovieEvent> firstThree;
        List<MovieEvent> SortedFirstThree = new ArrayList<>();

        SortedFirstThree.clear();
            // compare with current time and date and returmn

        for(int i=0;i<ListViewFragment.AllEvents.size();i++){
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
            Date dateEve = null;
            try {
                dateEve = sdf.parse(ListViewFragment.AllEvents.get(i).getEventStartTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long millis = dateEve.getTime();

            long eventTimeinMinutes = TimeUnit.MILLISECONDS.toMinutes(millis);
            long currentTime = TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis());

            if(currentTime < eventTimeinMinutes){
                SortedFirstThree.add(ListViewFragment.AllEvents.get(i));
            }


            }

        if(SortedFirstThree.size() > 3){
            firstThree = SortedFirstThree.subList(0,3);
        }
        else{
            firstThree = SortedFirstThree;
        }

        return firstThree;
    }
}
