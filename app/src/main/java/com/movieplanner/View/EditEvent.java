package com.movieplanner.View;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.movieplanner.Controller.Listener.DatePickerDialogListener;
import com.movieplanner.Controller.Listener.Miscelleneaous;
import com.movieplanner.Handler.ContactsDataHandler;
import com.movieplanner.Controller.Listener.ContactsDataListener;
import com.movieplanner.Handler.Database.DBoperations;
import com.movieplanner.Model.Attendees;
import com.movieplanner.Model.Movie;
import com.movieplanner.R;

import java.util.ArrayList;
import java.util.List;

public class EditEvent extends AppCompatActivity {

    //declare layout items
    private String eventID;
    private TextView editHeading;
    private  EditText editTitle;
    private  EditText editStartDate;
    private  EditText editEndDate;
    private  EditText editLocation;
    private  EditText editVenue;
    private  EditText editMovieName;
    private Button editEventSubmit;
    //private EditText attendeesField;
    private List<Attendees> attendees;
    private static final int RES_CODE_A = 3;
    Button addAttendee;

    // bind layout menu on top to display edit icons
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.actions_view_event, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_event);

        Toolbar toolbar = (Toolbar) findViewById(R.id.editToolbar);
        setSupportActionBar(toolbar);

        attendees = new ArrayList<>();

        setAllFields();

        // set listeners
        editMovieName.addTextChangedListener(mTextWatcher);
        editStartDate.addTextChangedListener(mTextWatcher);
    }

    // handle the options button click
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.view_event_edit:
                editDetails();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            checkEmptyFields();
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            checkEmptyFields();
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // check Fields For Empty Values
            checkEmptyFields();
        }
    };

    // set al layout items as disabled upon loading the activity
    private void setAllFields(){
        Intent newIntent = getIntent();

        // Values in data members

        eventID = newIntent.getStringExtra("eID");

        editTitle = findViewById(R.id.editEventTitle);
        editTitle.setText(newIntent.getStringExtra("eTitle"));
        editTitle.setEnabled(false);

        editHeading = findViewById(R.id.eventHeading);


        editStartDate = findViewById(R.id.editEventStartDate);
        editStartDate.setText(newIntent.getStringExtra("eStartDate"));
        new DatePickerDialogListener(this, editStartDate);
        editStartDate.setEnabled(false);

        editEndDate = findViewById(R.id.editEventEndDate);
        editEndDate.setText(newIntent.getStringExtra("eEndDate"));
        new DatePickerDialogListener(this, editEndDate);
        editEndDate.setEnabled(false);

        editVenue = findViewById(R.id.editEventVenue);
        editVenue.setText(newIntent.getStringExtra("eVenue"));
        editVenue.setEnabled(false);

        editLocation = findViewById(R.id.editEventLocation);
        editLocation.setText(newIntent.getStringExtra("eLocation"));
        editLocation.setEnabled(false);


        editMovieName = findViewById(R.id.editEventMovie);
        editMovieName.setText(newIntent.getStringExtra("mTitle"));
        editMovieName.setEnabled(false);



        editEventSubmit = findViewById(R.id.editEventSubmit);
        editEventSubmit.setVisibility(View.INVISIBLE);
        checkEmptyFields();

        addAttendee =  findViewById(R.id.addAttendee);
       // addAttendee.setText(newIntent.getStringExtra("mAttendees"));
        addAttendee.setEnabled(false);
        addAttendee.setOnClickListener(new ContactsDataListener(this));

        // add Attendees value to arraylist attendees
        if(newIntent.getStringExtra("mAttendees") != ""){
           updateAttendeesField(newIntent.getStringExtra("mAttendees"));
        }

    }

    private void checkEmptyFields(){
        //Log.i("ccb",editMovieName.getText().toString());
        if(editMovieName.getText().toString().equals("") || editStartDate.getText().toString().equals("")){
            editEventSubmit.setEnabled(false);
        }
        else{
            editEventSubmit.setEnabled(true);
        }
    }

    private void updateAttendeesField1(){

        LinearLayout layout = (LinearLayout) findViewById(R.id.attendeeLayout);
        // Check if layout already has elements
        if (((LinearLayout) layout).getChildCount() > 0) // if so remove
            ((LinearLayout) layout).removeAllViews();

        if (this.attendees != null && this.attendees.size() > 0) {
            // for each attendee
            for (Attendees attendee : attendees) {
                // create textviews and write to view
                TextView attendeeName = new TextView(this);
                // attendeeName.setLayoutParams(new LayoutParams());
                attendeeName.setText(attendee.getEmail());
                layout.addView(attendeeName);
                Button removeAttendee = new Button(this);
                //removeAttendee.setBackgroundResource(R.drawable.baseline_cancel_black_18dp);
                removeAttendee.setText("Remove");
                removeAttendee.setTag(attendee);
                removeAttendee.setWidth(60);
                removeAttendee.setHeight(110);
                removeAttendee.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        // remove attendee from the list
                        attendees.remove(v.getTag());
                        updateAttendeesField1();
                    }
                });
                layout.addView(removeAttendee);
            }
        } else {
            TextView noAttendees = new TextView(this);
            noAttendees.setText("No Attendees");
            layout.addView(noAttendees);
        }
    }
    // update the list with the contact names derived from contact manager
    private void updateAttendeesField(String val)
    {
        String[] attVal = val.split(",");

        for(int i=0;i<attVal.length;i++){
            if(attVal[i] != ""){
                attendees.add(new Attendees(null, attVal[i]));
            }

        }

        LinearLayout layout = (LinearLayout) findViewById(R.id.attendeeLayout);
        // Check if layout already has elements
        if (((LinearLayout) layout).getChildCount() > 0) // if so remove
            ((LinearLayout) layout).removeAllViews();

        if (this.attendees != null && this.attendees.size() > 0) {
            // for each attendee
            for (Attendees attendee : attendees) {
                // create textviews and write to view
                TextView attendeeName = new TextView(this);
                // attendeeName.setLayoutParams(new LayoutParams());
                attendeeName.setText(attendee.getEmail());
                layout.addView(attendeeName);
                Button removeAttendee = new Button(this);
                //removeAttendee.setBackgroundResource(R.drawable.baseline_cancel_black_18dp);
                removeAttendee.setText("Remove");
                removeAttendee.setTag(attendee);
                removeAttendee.setWidth(60);
                removeAttendee.setHeight(110);
                removeAttendee.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        // remove attendee from the list
                        attendees.remove(v.getTag());
                        updateAttendeesField1();
                    }
                });
                layout.addView(removeAttendee);
            }
        } else {
            TextView noAttendees = new TextView(this);
            noAttendees.setText("No Attendees");
            layout.addView(noAttendees);
        }
    }

    //enable all layout items when edit icon is clicked
    public void editDetails(){

        editHeading.setText("Edit Event Details");
        editTitle.setEnabled(true);
        editVenue.setEnabled(true);
        editEndDate.setEnabled(true);
        editStartDate.setEnabled(true);
        editLocation.setEnabled(true);
        editEventSubmit.setVisibility(View.VISIBLE);
        addAttendee.setEnabled(true);
        addAttendee.setOnClickListener(new ContactsDataListener(this));
        editMovieName.setEnabled(true);

        editMovieName.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(EditEvent.this, ViewMovies.class);
                startActivityForResult(intent, 2);
                    }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        {
            if (resultCode == RESULT_OK)
            {
                ContactsDataHandler contactsManager = new ContactsDataHandler(
                        this, data);
                String name = "";
                String email = "";
                try
                {
                    name = contactsManager.getContactName();
                    email = contactsManager.getContactEmail();
                }
                catch (ContactsDataHandler.ContactQueryException e)
                {
                }
                attendees.add(new Attendees(name, email));
                updateAttendeesField1();
            }
            else if (resultCode == RES_CODE_A){
                editMovieName.setText(data.getStringExtra("mName"));
                Toast.makeText(getApplicationContext(),"Movie Selected",Toast.LENGTH_LONG).show();

            }
        }
    }

    // Method for Edit events
    public void editEvent(View view){

        // MainActivity Loads
        Intent MainIntent = new Intent(EditEvent.this,
                MainActivity.class);

        // killing all previous activities
        MainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // update in Database

        DBoperations updateSingleEvent = new DBoperations();
        // add events to Database
        SQLiteDatabase mDatabase = MainActivity.sqlitehelper.getWritableDatabase();

        // get Movie object
        Movie movieObj = Miscelleneaous.findMovieObjByID(ViewMovies.list, editMovieName.getText().toString());

        boolean updateStatusinDB = updateSingleEvent.updateEvent(mDatabase,this.eventID, editTitle.getText().toString(), editStartDate.getText().toString(),
                editEndDate.getText().toString(), editVenue.getText().toString(),editLocation.getText().toString(),
                movieObj.getId(),attendees);

        if(updateStatusinDB){
            // Edit Main arraylist after finding the id
            for(int i=0;i<(ListViewFragment.AllEvents.size());i++){
                if(eventID.equals(ListViewFragment.AllEvents.get(i).getEventId())){

                    // editing event info
                    ListViewFragment.AllEvents.get(i).setEventTitle(editTitle.getText().toString());
                    ListViewFragment.AllEvents.get(i).setStartDate(editStartDate.getText().toString());
                    ListViewFragment.AllEvents.get(i).setVenue(editVenue.getText().toString());
                    ListViewFragment.AllEvents.get(i).setEndDate(editEndDate.getText().toString());
                    ListViewFragment.AllEvents.get(i).setLocation(editLocation.getText().toString());

                    // find movie object and pass below


                    ListViewFragment.AllEvents.get(i).setMoviedetails(movieObj);
                    ListViewFragment.AllEvents.get(i).setContacts(attendees);
                    // break the loop after changing
                    break;
                }
            }
            startActivity(MainIntent);
        }
        else{
            Toast.makeText(getApplicationContext(),"Failed", Toast.LENGTH_SHORT).show();
        }

    }
}

