package com.movieplanner.View;

import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.TabItem;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.movieplanner.Adapter.PageAdapter;
import com.movieplanner.Handler.Database.DB;
import com.movieplanner.R;
import com.movieplanner.Service.ConnectivityMgr;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;
    PageAdapter pageAdapter;
    TabItem tabListView;
    TabItem tabCalendar;
    TabItem tabMapView;
    public static boolean checkNetworkStatus = true;

    // DB datamembers
    public static  DB sqlitehelper; // public so can use same reference in different class
    private SQLiteDatabase mDatabase;


    // load the tab layout view with in the main activity page
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // run my DB operations
        performDBoperations();

        Log.d("MainActivityDB",sqlitehelper.toString());

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // bind tab elements
        tabListView = findViewById(R.id.tabListView);
        tabMapView = findViewById(R.id.tabMap);
        tabCalendar = findViewById(R.id.tabCalendar);
        viewPager = findViewById(R.id.viewPager);

        tabLayout = findViewById(R.id.tabLayout);
        pageAdapter = new PageAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pageAdapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        // tab layout listener to set colour on tabs on select and unselect
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 1) {
                    toolbar.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.color_skyBlue));
                    tabLayout.setBackgroundColor(ContextCompat.getColor(MainActivity.this,
                            R.color.colorAccent));
                } else if (tab.getPosition() == 2) {
                   toolbar.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.color_skyBlue));
                    tabLayout.setBackgroundColor(ContextCompat.getColor(MainActivity.this,
                            R.color.colorAccent));
                }else if (tab.getPosition() == 3) {
                    toolbar.setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.color_skyBlue));
                    tabLayout.setBackgroundColor(ContextCompat.getColor(MainActivity.this,
                            R.color.colorAccent));
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }


        });




    }

    @Override
    protected void onResume() {
        super.onResume();

        if(checkNetworkStatus){
            getApplicationContext().registerReceiver(new ConnectivityMgr(), new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        }

    }

    private void performDBoperations(){
        // initializing
        sqlitehelper = new DB(getApplicationContext());

        mDatabase = sqlitehelper.getReadableDatabase();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.actions_preferences, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent preferencesIntent = new Intent(
                getApplicationContext(), PreferencesActivity.class);
        getApplicationContext().startActivity(preferencesIntent);

        return super.onOptionsItemSelected(item);
    }
}
