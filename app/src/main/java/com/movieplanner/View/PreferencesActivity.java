package com.movieplanner.View;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.movieplanner.R;
import com.movieplanner.Service.DistanceMatrixIntentService;
import com.movieplanner.Service.ServiceManager;

import static com.movieplanner.Service.DistanceMatrixIntentService.DEFAULT_REMIND_AGAIN_INTERVAL_MS;

public class PreferencesActivity extends AppCompatActivity
{

    private ServiceManager serviceManager;
    private SharedPreferences prefs;
    private EditText notifThresholdField;
    private EditText distanceCheckFreqField;
    private EditText remindAgainField;


    //load the xml file and initialize the preferences
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);

        serviceManager = ServiceManager.getSingletonInstance(this);
        prefs =  PreferenceManager.getDefaultSharedPreferences(
                getApplicationContext());

        populateFields();

        Button submitPreference = findViewById(R.id.submitPreference);
        submitPreference.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                savePreferences();
            }
        });
    }

    //set the threshold field and distance check fields with current user preference values
    private void populateFields()
    {
        notifThresholdField = (EditText) findViewById(R.id.notif_threshold_field);
        notifThresholdField.setText(
                String.valueOf(prefs.getLong("notification_threshold",
                        DistanceMatrixIntentService.DEFAULT_THRESHOLD_MS)
                        / 1000 / 60));

        distanceCheckFreqField = (EditText) findViewById(R.id.distance_check_freq_field);
        distanceCheckFreqField.setText(
                String.valueOf(prefs.getLong("distance_check_freq_interval",
                        DistanceMatrixIntentService.DEFAULT_INTERVAL_MS)
                        / 1000 / 60));

        remindAgainField = (EditText) findViewById(R.id.remind_again_notify_field);
        remindAgainField.setText(
                String.valueOf(prefs.getLong("remind_again_check_interval",
                        DEFAULT_REMIND_AGAIN_INTERVAL_MS)
                        / 1000 / 60));

    }

    // upon submit of the preference save the values
    private void savePreferences()
    {

        // MainActivity Loads
        Intent MainIntent = new Intent(PreferencesActivity.this,
                MainActivity.class);

        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor prefsEditor = prefs.edit();
        prefsEditor.putLong("notification_threshold",
                Long.valueOf(notifThresholdField.getText().toString())
                        * 60 * 1000);
        prefsEditor.putLong("distance_check_freq_interval",
                Long.valueOf(distanceCheckFreqField.getText().toString())
                        * 60 * 1000);

        prefsEditor.putLong("remind_again_check_interval",
                Long.valueOf(remindAgainField.getText().toString())
                        * 60 * 1000);

        prefsEditor.apply();

        // Restart background service with new values
        serviceManager.startBackgroundDistanceMatrixService(getApplicationContext());
        Toast.makeText(getApplicationContext(),"Preferences saved successfully", Toast.LENGTH_SHORT).show();
        startActivity(MainIntent);

    }
}
