package com.movieplanner.View;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.movieplanner.Handler.Database.DBoperations;
import com.movieplanner.R;
import com.movieplanner.Service.RemindAgainNotificationReceiver;

public class CancelEventNotificationActivity extends Activity {

    private TextView eventName;
    private String eventID;
    private String eventTitle;

    private int notificationId;

    private TextView eventLocation;

    private Button cancelSubmit;
    private Button do_nothing;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cancel_event_summary);




        populateFields();

        NotificationManager notificationManager = (NotificationManager) getApplicationContext()
                .getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.cancel(notificationId);


    }

    private void populateFields(){
        Intent newIntent = getIntent();

        //Toast.makeText(getApplicationContext(),"EventId-"+newIntent.getStringExtra("eventID"), Toast.LENGTH_SHORT).show();
        // get data from intent
        eventID = newIntent.getStringExtra("eventID");
        eventTitle = newIntent.getStringExtra("eventTitle");
        notificationId = newIntent.getIntExtra("noti_id",0);

        eventName = findViewById(R.id.canceleventTitle);
        eventName.setText(newIntent.getStringExtra("eventName"));

        eventLocation = findViewById(R.id.eventLocation);
        eventLocation.setText(newIntent.getStringExtra("eventVenue"));

        cancelSubmit = findViewById(R.id.CancelSubmit);
        do_nothing = findViewById(R.id.do_nothing);


    }

    // on cancel submit button click
    public void cancelSubmit(View view){
        // MainActivity Loads
        Intent MainIntent = new Intent(CancelEventNotificationActivity.this,
                MainActivity.class);

        // delete this event

        DBoperations DBoperation = new DBoperations();

        // add events to Database
        SQLiteDatabase mDatabase = MainActivity.sqlitehelper.getWritableDatabase();
        int indexInList =0;
        // find event from All Event list
        for(int i=0;i<ListViewFragment.AllEvents.size();i++){

            if(this.eventID.equals(ListViewFragment.AllEvents.get(i).getEventId())){

                indexInList = i;

            }
        }
        boolean success = DBoperation.deleteEvents(mDatabase, this.eventID);

        if(success){
            ListViewFragment.AllEvents.remove(indexInList);
            Toast.makeText(getApplicationContext(),"Event Removed Successfully", Toast.LENGTH_SHORT).show();
            startActivity(MainIntent);
        }
        else{
            Toast.makeText(getApplicationContext(),"Failed", Toast.LENGTH_SHORT).show();
        }



    }

    public void do_nothing(View view){


        Intent MainIntent = new Intent(CancelEventNotificationActivity.this,
                MainActivity.class);
        Toast.makeText(getApplicationContext(),"Cancel Selected", Toast.LENGTH_SHORT).show();
        startActivity(MainIntent);
    }
}
