package com.movieplanner.View;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.movieplanner.Adapter.CalendarEventAdapter;
import com.movieplanner.Controller.Listener.CalendarItemSelectedListener;
import com.movieplanner.Model.MovieEvent;
import com.movieplanner.R;
import com.movieplanner.Service.ConnectivityMgr;
import com.movieplanner.Service.ServiceManager;


import java.util.Calendar;
import java.util.List;

public class CalendarFragment extends Fragment
{
    private static final int PERMISSION_REQUEST_LOC = 1001;

    private CalendarView calendarView;
    private GridView calendarGrid;
    private ListView eventList;
    private Calendar selectedDate;
    CalendarEventAdapter arrayAdapter;


    private ConnectivityMgr networkChangeReceiver = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        View v = inflater.inflate(R.layout.activity_calendar, container, false);

        return v;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Refresh fragment
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

//        if(MainActivity.checkNetworkStatus){
//            this.getContext().registerReceiver(new ConnectivityMgr(), new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
//        }
    }

    @Override
    public void onPause() {
        //this.getContext().unregisterReceiver(new ConnectivityMgr());
        super.onPause();
    }


    //load the customer calendar components in the grid
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        super.onCreate(savedInstanceState);

        setUpFloatingActionButton();
        calendarView = (CalendarView) getView().findViewById(R.id.calendar);
        calendarGrid = (GridView) getView().findViewById(R.id.calendar_grid);
        eventList = (ListView) getView().findViewById(R.id.calendar_event_list);
        eventList.setEmptyView(getView().findViewById(R.id.empty_list_text));

        List<MovieEvent> selectedEvents = CalendarView.selectedEvents;


        calendarGrid.setOnItemClickListener(

                new CalendarItemSelectedListener(
                        getActivity(), calendarView, eventList, arrayAdapter));


        if (ContextCompat.checkSelfPermission(
                getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_REQUEST_LOC);
        }

        //serviceManager.startBackgroundDistanceMatrixService(getActivity().getApplicationContext());





    }
    private BroadcastReceiver networkStateReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


            ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo ni = manager.getActiveNetworkInfo();
            //doSomethingOnNetworkChange(ni);
        }
    };


    // add floating bar for add event from calender
    private void setUpFloatingActionButton()
    {
        FloatingActionButton fab = (FloatingActionButton) getView().findViewById(R.id.fab_add);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent addEventIntent = new Intent(getActivity(), AddNewEvent.class);
                startActivity(addEventIntent);
            }
        });
    }
}
