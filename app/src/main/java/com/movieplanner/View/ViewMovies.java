package com.movieplanner.View;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.movieplanner.Adapter.MoviesAdapter;
import com.movieplanner.Handler.FileHandler;
import com.movieplanner.Model.Movie;
import com.movieplanner.R;

import java.util.ArrayList;
import java.util.List;

public class ViewMovies extends AppCompatActivity {
    private Context context;
    public static List<Movie> list =  new ArrayList<>();

    //recyclerview objects
    private RecyclerView moviesRecyclerView;
    private RecyclerView.Adapter adapter;

    //create new event view
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movies_view);
        context = this;

        //initializing views
        moviesRecyclerView = (RecyclerView) findViewById(R.id.moviesRecyclerView);
        moviesRecyclerView.setHasFixedSize(true);
        moviesRecyclerView.setLayoutManager(new LinearLayoutManager(this));



        adapter = new MoviesAdapter(list, this);
        moviesRecyclerView.setAdapter(adapter);

        //loadRecyclerViewItem();
    }

    private void loadRecyclerViewItem() {
        // call filehandler class method to generate events details in card layout
//        FileHandler fileHandler = new FileHandler();
//        Movie myList;
//
//        List<Movie> moviesData = fileHandler.parseMoviesFile(context);
//        for (int i = 0; i <  moviesData.size(); i++) {
//            myList = new Movie(
//                    moviesData.get(i).getId(),
//                    moviesData.get(i).getTitle(),
//                    moviesData.get(i).getYear(),
//                    moviesData.get(i).getPoster()
//            );
//
//            list.add(myList);
//        }

        adapter = new MoviesAdapter(list, this);
        moviesRecyclerView.setAdapter(adapter);
    }
}
