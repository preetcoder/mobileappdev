package com.movieplanner.Controller.Listener;

import android.util.Log;

import com.movieplanner.Model.Attendees;
import com.movieplanner.Model.Movie;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Miscelleneaous {

    // Static Attendees list
    public static List<Attendees> AllAttendees = new ArrayList<>();

    // Method for getting movie object from string
    public static Movie findMovieObjByID(List<Movie> list, String MovieName){

        Log.i("mnmnmn",list.get(0).getId());

        Movie singleMovieObj = new Movie();

        for(int i=0;i<list.size();i++){
            if(MovieName.equals(list.get(i).getTitle())){
                singleMovieObj = list.get(i);
            }
        }

        return singleMovieObj;
    }

    // method for contact separations
    public static String[] findContacts(String AttendeesVal){

        String[] contactsval = new String[10];
        contactsval = AttendeesVal.split(",");

        return contactsval;
    }

    public static Date convertStringToDate(String dateInString){
        DateFormat format = new SimpleDateFormat("d/MM/yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


}
