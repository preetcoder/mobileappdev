package com.movieplanner.Controller.Listener;

import android.app.TimePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.TimePicker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class TimePickerDialogListener
        implements TimePickerDialog.OnTimeSetListener, View.OnClickListener
{
    private Context context;
    private EditText timeField;
    private Calendar time;
    private DateFormat formatter = new SimpleDateFormat("h:mm:ss a",
            Locale.getDefault());

    //constructor
    public TimePickerDialogListener(Context context, EditText timeField)
    {
        this.context = context;
        this.timeField = timeField;
        this.timeField.setOnClickListener(this);
        this.time = Calendar.getInstance();
    }

    // set details to be shown on the time picker layout
    @Override
    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute)
    {
        int count = 0;
        time.set(Calendar.HOUR_OF_DAY, hourOfDay);
        time.set(Calendar.MINUTE, minute);
        timeField.append(" "+formatter.format(time.getTime()));

        new DatePickerDialogListener(context, timeField);
    }

    // instantiate TimePicker with the layout field values
    @Override
    public void onClick(View view)
    {
        new TimePickerDialog(
                context, this, time.get(Calendar.HOUR_OF_DAY),
                time.get(Calendar.MINUTE), false)
                .show();
    }
}

